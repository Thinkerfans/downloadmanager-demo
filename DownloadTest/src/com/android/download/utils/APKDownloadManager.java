package com.android.download.utils;

import java.io.File;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import android.annotation.SuppressLint;
import android.app.DownloadManager;
import android.app.DownloadManager.Query;
import android.app.DownloadManager.Request;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.text.TextUtils;
import android.widget.Toast;

import com.android.common.utils.Config;
import com.android.common.utils.LogUtil;

@SuppressLint("NewApi")
public class APKDownloadManager {

	public static ConcurrentHashMap<Long, String> sAPKMap = new ConcurrentHashMap<Long, String>();

	public static void downApk(Context c, String apkUrl) throws Exception {
		if (TextUtils.isEmpty(apkUrl) || !apkUrl.contains(".apk")) {
			throw new Exception("url error");
		}
		
		String apkName = parseApkName(apkUrl);
		if (TextUtils.isEmpty(apkName)) {
			Toast.makeText(c, "异常链接", Toast.LENGTH_SHORT).show();
			return;
		}

		if (isDownloading(apkName)) {
			Toast.makeText(c, "正在下载中", Toast.LENGTH_SHORT).show();
			return;
		}

		DownloadManager downloadManager = (DownloadManager) c
				.getSystemService(Context.DOWNLOAD_SERVICE);

		Uri uri = Uri.parse(apkUrl);
		Request request = new Request(uri);
		request.setAllowedNetworkTypes(Request.NETWORK_WIFI);
		// 禁止发出通知，既后台下载，如果要使用这一句必须声明一个权限：android.permission.DOWNLOAD_WITHOUT_NOTIFICATION
		// request.setShowRunningNotification(false);
		// 不显示下载界面
		request.setVisibleInDownloadsUi(false);
		File f = new File(Config.APK_DOWNLOAD_DIR, apkName);
		if(!f.getParentFile().exists()){
			f.getParentFile().mkdirs();
		}
		
		if (f.exists()) {
			f.delete();
		}
		LogUtil.e("downApk : ", "f.exists() :" + f.exists() + "name :" + apkName);
		request.setDestinationInExternalPublicDir("download/.apks/", apkName);
		long id = downloadManager.enqueue(request);
		sAPKMap.put(id, apkName);

		LogUtil.e("downApk : ", "id :" + id + "name :" + apkName);

	}

	private static boolean isDownloading(String name) {
		for (String s : sAPKMap.values()) {
			if (s.equals(name)) {
				return true;
			}
		}
		return false;
	}

	private static String parseApkName(String apkUrl) {
		int start = apkUrl.lastIndexOf("/");
		int end = apkUrl.lastIndexOf(".apk");
		return apkUrl.substring(start + 1, end + 4);
	}

}

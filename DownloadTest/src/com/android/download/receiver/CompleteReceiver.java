package com.android.download.receiver;

import java.io.File;

import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.text.TextUtils;

import com.android.common.utils.Config;
import com.android.common.utils.LogUtil;
import com.android.download.utils.APKDownloadManager;

/**
 * 类说明：
 * 
 * @author Baker.li
 * @date 2014年2月25日
 * @version 1.0
 */

public class CompleteReceiver extends BroadcastReceiver {
	@Override
	public void onReceive(Context context, Intent intent) {

		String action = intent.getAction();
		if (DownloadManager.ACTION_DOWNLOAD_COMPLETE.equals(action)) {
			LogUtil.e("completeReceiver : ", "aciton :" + intent.getExtras());
			long id = intent
					.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);
			int status = intent.getIntExtra("extra_download_status",
					DownloadManager.STATUS_FAILED);
			String name = APKDownloadManager.sAPKMap.get(id);
			LogUtil.e("completeReceiver : ", "id :" + intent.getExtras()
					+ "name :" + name + "status : " + status);
			if (!TextUtils.isEmpty(name)) {
				queryDownloadStatus(context, id, name);
			}
		}
	}

	private void queryDownloadStatus(Context context, long id, String name) {
		DownloadManager downloadManager = (DownloadManager) context
				.getSystemService(Context.DOWNLOAD_SERVICE);
		DownloadManager.Query query = new DownloadManager.Query();
		query.setFilterById(id);
		Cursor c = downloadManager.query(query);
		if (c.moveToFirst()) {
			int status = c.getInt(c
					.getColumnIndex(DownloadManager.COLUMN_STATUS));
			
			LogUtil.e("completeReceiver : ", "id :"
					+ "name :" + name + "status : " + status);
			switch (status) {
			case DownloadManager.STATUS_SUCCESSFUL:
			Intent it = new Intent(Intent.ACTION_VIEW);
				it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				it.setDataAndType(
						Uri.fromFile(new File(Config.APK_DOWNLOAD_DIR, name)),
						"application/vnd.android.package-archive");		
				context.startActivity(it);
				APKDownloadManager.sAPKMap.remove(id);
				break;
			case DownloadManager.STATUS_FAILED:
				downloadManager.remove(id);
				APKDownloadManager.sAPKMap.remove(id);
				File erroFile = new File(Config.APK_DOWNLOAD_DIR, name);
				erroFile.delete();
				break;
			default:
				break;
			}
		}
		c.close();
	}
}
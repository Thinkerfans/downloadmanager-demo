package com.android.download.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.Map;

import org.apache.http.HttpStatus;

import android.content.Context;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;

import com.android.common.utils.Config;
import com.android.common.utils.NetTools;

import download.test.BuildConfig;

public class DownloadApkTsk extends AsyncTask<Void, Long, Void> {
	private static final String TAG = "DownloadApkTsk";

	public interface OnDownloadApkListener {
		public void onDownloadStarted(DownloadApkTsk tsk, String apkName,
				int sequenceNo);

		public void onDownloadProgress(DownloadApkTsk tsk, int sequenceNo,
				String apkName, long downloadedCount, long totalCount);

		// You can add fail reason here
		public void onDownloadFail(DownloadApkTsk tsk, String apkName,
				int sequenceNo);

		public void onDownloadSuccess(DownloadApkTsk tsk, String apkName,
				int sequenceNo, String apkCachePath);
	}

	private Context mContext;
	private String mApkDownloadUrl;
	private String mApkName;
	private int mSequenceNo;
	private OnDownloadApkListener mCallback;

	private long mDownloadedCount;
	private long mTotalCount;
	private String mApkCachePath;

	private String mFileName;
	private String mFileSuffix;

	// update type constants
	private static final long UPDATE_TYPE_STARTED = 0L;
	private static final long UPDATE_TYPE_PROGRESS = 1L;

	/**
	 * 超时时间 30秒
	 * */
	private final static int TIME_OUT = 30 * 1000;
	/**
	 * 每次接受256K
	 * */
	private final static int RECV_BUF_SIZE = 256 * 1024;

	public DownloadApkTsk(Context context, String apkDownloadUrl,
			String apkName, int sequenceNo) {
		mContext = context;
		mApkDownloadUrl = apkDownloadUrl;
		mApkName = apkName;
		mSequenceNo = sequenceNo;
		mDownloadedCount = 0;
		mTotalCount = 0;
	}

	public void setCallback(OnDownloadApkListener callback) {
		mCallback = callback;
	}

	@Override
	protected Void doInBackground(Void... params) {
		publishProgress(UPDATE_TYPE_STARTED);
		mApkCachePath = doHttpRequest();
		return null;
	}

	/**
	 * first value is type.
	 * <p>
	 * if type is {@link #UPDATE_TYPE_PROGRESS}
	 * <p>
	 * the second value is {@link #mDownloadedCount}
	 * <p>
	 * and the third value is {@link #mTotalCount}
	 * <p>
	 */
	@Override
	protected void onProgressUpdate(Long... values) {
		super.onProgressUpdate(values);
		long type = values[0];
		if (type == UPDATE_TYPE_STARTED) {
			if (mCallback != null) {
				mCallback.onDownloadStarted(this, mApkName, mSequenceNo);
			}
		} else if (type == UPDATE_TYPE_PROGRESS) {
			if (mCallback != null) {
				mCallback.onDownloadProgress(this, mSequenceNo, mApkName,
						mDownloadedCount, mTotalCount);
			}
		}
	}

	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		if (mCallback != null) {
			if (mApkCachePath == null) {
				mCallback.onDownloadFail(this, mApkName, mSequenceNo);
			} else {
				mCallback.onDownloadSuccess(this, mApkName, mSequenceNo,
						mApkCachePath);
			}
		}
	}

	public String getDownloadUrl() {
		return mApkDownloadUrl;
	}

	// /////////////////////////////////////////////////////////////////////////////
	/**
	 * trigger actual download acion
	 */
	// ////////////////////////////////////////////////////////////////////////////
	private String doHttpRequest() {
		HttpURLConnection conn = null;
		long downloadedLength = 0L;
		File cacheFile = null;
		try {
			URL url = new URL(mApkDownloadUrl);
			conn = (HttpURLConnection) url.openConnection();
		} catch (Exception e) {
			if (BuildConfig.DEBUG) {
				Log.e(TAG, "Exception", e);
			}
			return null;
		}
		try {
			// 设置http请求
			conn.setConnectTimeout(TIME_OUT);
			conn.setReadTimeout(TIME_OUT);
			conn.setRequestMethod("GET");
			NetTools.setCommonHttpHeader(conn);
			conn.connect(); // 发起链接
		} catch (SocketTimeoutException e) {
			if (BuildConfig.DEBUG) {
				Log.e(TAG, "SocketTimeoutException", e);
			}
			conn.disconnect();
			return null;
		} catch (Exception e) {
			if (BuildConfig.DEBUG) {
				Log.e(TAG, "Exception", e);
			}
			conn.disconnect();
			return null;
		}
		try {
			int statusCode = conn.getResponseCode();
			if (BuildConfig.DEBUG) {
				Log.e(TAG, "http status code:" + statusCode);
			}
			if (statusCode != HttpStatus.SC_OK) {
				conn.disconnect();
				return null;
			}
			Map<String, String> header = NetTools.getHttpResponseHeader(conn);
			String contentLengthStr = header.get("content-length");
			if (contentLengthStr == null
					|| TextUtils.equals(contentLengthStr.trim(), "")) {
				mTotalCount = 0;
			} else {
				mTotalCount = Long.valueOf(contentLengthStr);
			}
			if (mTotalCount == 0) {
				conn.disconnect();
				return null;
			}
		} catch (Exception e) {
			if (BuildConfig.DEBUG) {
				Log.e(TAG, "Exception", e);
			}
			conn.disconnect();
			return null;
		}
		InputStream is = null;
		FileOutputStream fos = null;
		try {
			is = conn.getInputStream();
			byte[] buffer = new byte[RECV_BUF_SIZE];
			int len = 0;
			cacheFile = createSaveFile();
			if (cacheFile == null) {
				// create file fail
				return null;
			}
			fos = new FileOutputStream(cacheFile);
			long lastReceive = System.currentTimeMillis();
			while (!isCancelled() && (len = is.read(buffer)) != -1) {
				fos.write(buffer, 0, len);
				mDownloadedCount += len;
				long now = System.currentTimeMillis();
				if (now - lastReceive > 500 || mDownloadedCount == mTotalCount) {
					lastReceive = now;
					publishProgress(UPDATE_TYPE_PROGRESS);
				}
			}
		} catch (Exception e) {
			if (BuildConfig.DEBUG) {
				Log.e(TAG, "Exception", e);
			}
			conn.disconnect();
			return null;
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
					//
				}
			}
			if (fos != null) {
				try {
					fos.close();
				} catch (IOException e) {
					//
				}
			}
		}
		return cacheFile.getAbsolutePath();
	}

	private File createSaveFile() {
		String dir = Config.APK_DOWNLOAD_DIR;
		trimApkFileName();
		File f = new File(dir, mFileName + System.currentTimeMillis()
				+ mFileSuffix);
		// File f = new File(dir, System.currentTimeMillis() + ".apk");
		if (!f.getParentFile().exists()) {
			f.getParentFile().mkdirs();
		}
		// clean cache if exist
		File[] list = f.getParentFile().listFiles(new FilenameFilter() {

			@Override
			public boolean accept(File dir, String filename) {
				if (filename.startsWith(mFileName)) {
					return true;
				}
				return false;
			}
		});
		if (list != null && list.length > 0) {
			for (File file : list) {
				file.delete();
			}
		}
		if (f.exists()) {
			f.delete();
		}
		return f;
	}

	private void trimApkFileName() {
		int index = mApkDownloadUrl.lastIndexOf("/");
		String trim = mApkDownloadUrl.substring(index + 1);
		index = trim.indexOf(".");
		mFileName = trim.substring(0, index);
		mFileSuffix = trim.substring(index);
		index = mFileSuffix.indexOf("?");
		if (index != -1) {
			mFileSuffix = mFileSuffix.substring(0, index);
		}
	}

	@Override
	public boolean equals(Object o) {
		if (this.mApkDownloadUrl.equals(((DownloadApkTsk) o).getDownloadUrl())) {
			return true;
		} else {
			return false;
		}
	}
}

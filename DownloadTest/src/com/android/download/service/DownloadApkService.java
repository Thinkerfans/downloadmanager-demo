package com.android.download.service;

import java.util.ArrayList;
import java.util.List;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationCompat.Builder;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseArray;
import android.widget.RemoteViews;
import android.widget.Toast;

import com.android.common.utils.AppInfoModel;
import com.android.common.utils.AppInfoUtil;
import com.android.download.service.DownloadApkTsk.OnDownloadApkListener;

import download.test.BuildConfig;
import download.test.R;

public class DownloadApkService extends Service implements
		OnDownloadApkListener {
	private static final String TAG = "DownloadApkService";

	private static final String EXTRA_LATEST_APK_DOWNLOAD_URL = "extra_latest_apk_download_url";
	private static final String EXTRA_LATEST_APK_NAME = "extra_latest_apk_name";
	private static final String EXTRA_CANCEL_SEQUENCE_NO = "extra_cancel_sequence_no";
	private static final String EXTRA_CANCEL_APK_DOWNLOAD_URL = "extra_cancel_apk_download_url";

	private static final int MAX_PROGRESS_VALUE = 100;

	private int mMaxStartId;
	private SequenceGenFactory mSequenceGenFactory;

	private byte[] mTskLock = new byte[0];
	private List<DownloadApkTsk> mRunningTskList;
	private String mLatestApkDownloadUrl;
	private String mLatestApkName;

	private SparseArray<Builder> mNotificationMap;
	private NotificationManager mNm;

	/**
	 * interface provided for download apk
	 * <p>
	 * all you need to do is invoke this method.
	 * 
	 * @param context
	 * @param apkDownloadUrl
	 * @param apkName
	 */
	public static void triggerDownload(Context context, String apkDownloadUrl,
			String apkName) {
		Intent i = new Intent(context, DownloadApkService.class);
		i.putExtra(EXTRA_LATEST_APK_DOWNLOAD_URL, apkDownloadUrl);
		i.putExtra(EXTRA_LATEST_APK_NAME, apkName);
		context.startService(i);
	}

	public static PendingIntent constructPendingIntent(Context context,
			int sequenceNo, String apkDownloadUrl) {
		Intent i = new Intent(context, DownloadApkService.class);
		i.putExtra(EXTRA_CANCEL_SEQUENCE_NO, sequenceNo);
		i.putExtra(EXTRA_CANCEL_APK_DOWNLOAD_URL, apkDownloadUrl);
		return PendingIntent.getService(context, 0, i, 0);
	}

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		mSequenceGenFactory = SequenceGenFactory.getInstance();
		mRunningTskList = new ArrayList<DownloadApkTsk>();
		mNotificationMap = new SparseArray<Builder>();
		mNm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		mMaxStartId = startId;
		if (intent == null) {
			return super.onStartCommand(intent, flags, startId);
		}
		// for tsk cancel
		int sequenceNo = intent.getIntExtra(EXTRA_CANCEL_SEQUENCE_NO, -1);
		String apkDownloadUrl = intent
				.getStringExtra(EXTRA_CANCEL_APK_DOWNLOAD_URL);
		if (sequenceNo != -1 && apkDownloadUrl != null) {
			cancelTsk(sequenceNo, apkDownloadUrl);
			return START_STICKY;
		}
		// end
		mLatestApkDownloadUrl = intent
				.getStringExtra(EXTRA_LATEST_APK_DOWNLOAD_URL);
		mLatestApkName = intent.getStringExtra(EXTRA_LATEST_APK_NAME);
		if (mLatestApkDownloadUrl == null) {
			return super.onStartCommand(intent, flags, startId);

		}
		// enroll the download url
		addDownloadTsk(mLatestApkDownloadUrl, mLatestApkName);
		return START_STICKY;
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		mSequenceGenFactory.clean();
		mRunningTskList = null;
	}

	private void cancelTsk(int sequenceNo, String apkDownloadUrl) {
		mNm.cancel(sequenceNo);
		mNotificationMap.remove(sequenceNo);
		DownloadApkTsk tsk = retriveDownloadTask(apkDownloadUrl);
		Log.e(TAG, "cancelTskcancelTskcancelTskcancelTsk,sequenceNo : "
				+ sequenceNo + ",apkDownloadUrl : " + apkDownloadUrl+",tsk :"+tsk);
		if (tsk != null) {
			tsk.cancel(true);
		}
		ShutdownServiceIfNeeded(tsk);
	}

	private void addDownloadTsk(String apkDownloadUrl, String apkName) {
		// check if this apk is downloading
		synchronized (mTskLock) {
			for (DownloadApkTsk tsk : mRunningTskList) {
				if (tsk.getDownloadUrl().equals(apkDownloadUrl)) {
					if (BuildConfig.DEBUG) {
						Log.d(TAG, "this apk is downloading,please waiting.");
					}
					Toast.makeText(this, "正在下载中……", Toast.LENGTH_SHORT).show();
					return;
				}
			}
		}
		DownloadApkTsk tsk = new DownloadApkTsk(this, mLatestApkDownloadUrl,
				mLatestApkName, mSequenceGenFactory.genNo());
		tsk.setCallback(this);
		synchronized (mTskLock) {
			mRunningTskList.add(tsk);
		}
		tsk.execute();
	}

	private void ShutdownServiceIfNeeded(DownloadApkTsk tsk) {
		boolean removed = mRunningTskList.remove(tsk);
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "task removed : " + removed);
		}
		if (mRunningTskList.size() <= 0) {
			Log.d(TAG, "stop service because no task left");
			stopSelf(mMaxStartId);
		} else {
			Log.d(TAG,
					"keep service running because there are tasks executing.");
		}
	}

	private DownloadApkTsk retriveDownloadTask(String downloadApkUrl) {
		for (DownloadApkTsk tsk : mRunningTskList) {
			if (TextUtils.equals(tsk.getDownloadUrl(), downloadApkUrl)) {
				return tsk;
			}
		}
		return null;
	}

	/**
	 * download callback
	 */

	@Override
	public void onDownloadStarted(DownloadApkTsk tsk, String apkName,
			int sequenceNo) {
		// show the notification
		Log.e(TAG, "onDownloadStarted,tsk : " + tsk + ",sequenceNo : "
				+ sequenceNo);
		showNotification(tsk, apkName, sequenceNo);
	}

	@Override
	public void onDownloadProgress(DownloadApkTsk tsk, int sequenceNo,
			String apkName, long downloadedCount, long totalCount) {
		// update the notification
		updateNotification(tsk, sequenceNo, apkName, downloadedCount,
				totalCount);
	}

	@Override
	public void onDownloadFail(DownloadApkTsk tsk, String apkName,
			int sequenceNo) {
		// cancel the notification
		failNotification(apkName, sequenceNo);
		ShutdownServiceIfNeeded(tsk);
	}

	@Override
	public void onDownloadSuccess(DownloadApkTsk tsk, String apkName,
			int sequenceNo, String apkCachePath) {
		// define user's click action to launch apk installation
		successNotification(apkName, sequenceNo, apkCachePath);
		ShutdownServiceIfNeeded(tsk);
	}

	private int calcDownloadProgress(long downloadedCount, long totalCount) {
		return (int) (downloadedCount * MAX_PROGRESS_VALUE * 1.0f / totalCount);
	}

	/**
	 * show notification
	 */

	private void showNotification(DownloadApkTsk tsk, String apkName,
			int sequenceNo) {
		Builder builder = new NotificationCompat.Builder(this);
		mNotificationMap.append(sequenceNo, builder);
		// builder.setSmallIcon(R.drawable.ic_launcher)
		// .setTicker(getString(R.string.download_noti_ticker, apkName))
		// .setContentTitle(apkName).setAutoCancel(false)
		// .setProgress(MAX_PROGRESS_VALUE, 0, true);
		RemoteViews rv = new RemoteViews(getPackageName(), R.layout.noti_layout);
		rv.setTextViewText(R.id.percent, "");
		rv.setTextViewText(R.id.title,
				getString(R.string.download_noti_ticker, apkName));
		rv.setTextViewText(R.id.cancel_btn, getString(R.string.cancel));
		rv.setProgressBar(android.R.id.progress, MAX_PROGRESS_VALUE, 0, true);
		rv.setOnClickPendingIntent(R.id.cancel_btn,
				constructPendingIntent(this, sequenceNo, tsk.getDownloadUrl()));
		builder.setContent(rv)
				.setTicker(getString(R.string.download_noti_ticker, apkName))
				.setSmallIcon(android.R.drawable.stat_sys_download);

		Notification notification = builder.build();
		// notification.flags |= Notification.FLAG_FOREGROUND_SERVICE;
		mNm.notify(sequenceNo, notification);
	}

	private void updateNotification(DownloadApkTsk tsk, int sequenceNo,
			String apkName, long downloadedCount, long totalCount) {
		Builder builder = mNotificationMap.get(sequenceNo);
		if (builder != null) {
			int percent = calcDownloadProgress(downloadedCount, totalCount);
			RemoteViews rv = new RemoteViews(getPackageName(),
					R.layout.noti_layout);
			rv.setTextViewText(R.id.percent, percent + "%");
			rv.setTextViewText(R.id.title,
					getString(R.string.download_noti_ticker, apkName));
			rv.setTextViewText(R.id.cancel_btn, getString(R.string.cancel));
			rv.setProgressBar(android.R.id.progress, MAX_PROGRESS_VALUE,
					percent, false);
			builder.setContent(rv)
					.setTicker(
							getString(R.string.download_noti_ticker, apkName))
					.setSmallIcon(android.R.drawable.stat_sys_download);
			mNm.notify(sequenceNo, builder.build());
		}
	}

	private void successNotification(String apkName, int sequenceNo,
			String apkCachePath) {
		mNotificationMap.delete(sequenceNo);
		Builder builder = new NotificationCompat.Builder(this);
		String txt = getString(R.string.download_noti_success_ticker, apkName);
		String toastTxt = getString(R.string.download_noti_success_toast,
				apkName);
		AppInfoModel model = AppInfoUtil.getApkInfo(this.getApplicationContext(), apkCachePath);
		Bitmap icon = null;
		if (model != null) {
			icon = AppInfoModel.drawableToBitmap(model.getApkIcon());
		}

		if (icon != null) {
			builder.setLargeIcon(icon);
		} else {
			builder.setSmallIcon(R.drawable.ic_downing);
		}
		builder.setTicker(txt).setContentTitle(apkName).setAutoCancel(true)
				.setContentText(toastTxt)
				.setContentIntent(constructConntentIntent(apkCachePath));
		Notification notification = builder.build();
		notification.flags |= Notification.FLAG_AUTO_CANCEL;
		mNm.notify(sequenceNo, notification);

		Intent i = new Intent();
		i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		i.setAction(Intent.ACTION_VIEW);
		i.setDataAndType(Uri.parse("file://" + apkCachePath),
				"application/vnd.android.package-archive");
		startActivity(i);
	}

	private void failNotification(String apkName, int sequenceNo) {
		mNotificationMap.delete(sequenceNo);
		mNm.cancel(sequenceNo);
		Toast.makeText(this,
				getString(R.string.download_noti_fail_txt, apkName),
				Toast.LENGTH_SHORT).show();
	}

	private PendingIntent constructConntentIntent(String apkCachedPath) {
		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.addCategory(Intent.CATEGORY_DEFAULT);
		intent.setDataAndType(Uri.parse("file://" + apkCachedPath),
				"application/vnd.android.package-archive");
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		PendingIntent pi = PendingIntent.getActivity(this, 0, intent,
				PendingIntent.FLAG_UPDATE_CURRENT);
		return pi;
	}

	/**
	 * 
	 * gen the id used to identify the notification and task
	 * 
	 */
	private static class SequenceGenFactory {
		private int mSequenceNo;
		private static final int MAX_SEQUENCE_NO = 10000;

		private static SequenceGenFactory mInstance = null;

		public static SequenceGenFactory getInstance() {
			if (mInstance == null) {
				synchronized (SequenceGenFactory.class) {
					if (mInstance == null) {
						mInstance = new SequenceGenFactory();
					}
				}
			}
			return mInstance;
		}

		public void clean() {
			mInstance.mSequenceNo = 0;
			mInstance = null;
		}

		private SequenceGenFactory() {
			mSequenceNo = 0;
		}

		public int genNo() {
			return (mSequenceNo++) % MAX_SEQUENCE_NO;
		}
	}
}

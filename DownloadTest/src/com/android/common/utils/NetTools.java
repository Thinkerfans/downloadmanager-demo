package com.android.common.utils;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.MessageDigest;
import java.security.cert.CertificateFactory;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.NetworkInfo.State;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.telephony.TelephonyManager;

/**
 * 类说明：
 * 
 * @author xinhui.cheng
 * @date 2013-4-7
 * @version 1.0
 */
public class NetTools {
	
	
	public static boolean isMobileState(Context c){
		ConnectivityManager cm = (ConnectivityManager) c.getSystemService(Context.CONNECTIVITY_SERVICE);
		State mobileState = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState();  
		State wifiState = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState();
		if((mobileState != null && State.CONNECTED == mobileState) 
				&&  (wifiState == null || State.CONNECTED != wifiState)){
			return true;

		}
		return false;
	}
	
	/**
	 * 是否联网
	 * 
	 * @param context
	 * @return true为有联网反之没有联网
	 */
	public static boolean isNetworkAvailable(Context context) {
		ConnectivityManager mgr = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (mgr != null) {
			NetworkInfo[] info = mgr.getAllNetworkInfo();
			if (info != null) {
				for (int i = 0; i < info.length; i++) {
					if (info[i].getState() == NetworkInfo.State.CONNECTED) {
						return true;
					}
				}
			}
		} else {
			return false;
		}
		return false;
	}

	/**
	 * 判断当前网络连接是否为cmwap
	 * 
	 * @param context
	 * @return
	 */
	public static boolean isCmwap(Context activity) {
		ConnectivityManager manager = (ConnectivityManager) activity
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netWrokInfo = manager.getActiveNetworkInfo();

		if (netWrokInfo == null) {
			return false;
		}

		String typeName = netWrokInfo.getTypeName();
		String extraInfo = netWrokInfo.getExtraInfo();

		if ("MOBILE".equalsIgnoreCase(typeName)
				&& ("cmwap".equalsIgnoreCase(extraInfo)
						|| "3gwap".equalsIgnoreCase(extraInfo)
						|| "uniwap".equalsIgnoreCase(extraInfo) || "ctwap"
							.equalsIgnoreCase(extraInfo))) {
			return true;
		} else {

			return false;
		}
	}

	public static HttpURLConnection getCmwapConnect(String requestUrl)
			throws IOException {
		URL url = new URL(requestUrl);
		HttpURLConnection con;

		LogUtil.i("Tools", "use cmwap...");

		String host = url.getHost();
		int port = url.getPort();
		if (port == -1) {
			requestUrl = requestUrl.replaceAll(host, "10.0.0.172:80");
		} else {
			requestUrl = requestUrl.replaceAll(host + ":" + port,
					"10.0.0.172:80");
		}

		url = new URL(requestUrl);
		con = (HttpURLConnection) url.openConnection();

		String xOnlineHost = null;
		if (port == -1) {
			xOnlineHost = host;
		} else {
			xOnlineHost = host + ":" + port;
		}
		con.setRequestProperty("Host", "10.0.0.172");
		con.setRequestProperty("X-Online-Host", xOnlineHost);
		return con;
	}

	public static void setCommonHttpHeader(HttpURLConnection con) {
		con.setRequestProperty("Accept", "*/*");
		con.setRequestProperty("Accept-Language", "zh-CN, zh");
		con.setRequestProperty("Charset",
				"UTF-8,ISO-8859-1,US-ASCII,ISO-10646-UCS-2;q=0.6");
		con.setRequestProperty(
				"User-Agent",
				"Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.2; Trident/4.0; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30; .NET CLR 3.0.4506.2152; .NET CLR 3.5.30729)");
		con.setRequestProperty("Connection", "Keep-Alive");
	}

	public static Map<String, String> getHttpResponseHeader(
			HttpURLConnection http) {
		Map<String, String> header = new LinkedHashMap<String, String>();
		for (int i = 0;; i++) {
			String mine = http.getHeaderField(i);
			if (mine == null)
				break;

			String str = http.getHeaderFieldKey(i);
			if (str != null) {
				header.put(str.toLowerCase(Locale.CHINA), mine);
			}
		}
		return header;
	}
	
	//==========  测试时把常量DEBUG设为true;否则请设为false  ================
	public static final boolean DEBUG = true;
	//==========  测试时把常量DEBUG设为true;否则请设为false  ================
	public static String getServerUrl(){
      return "http://test.leyogame.cn:8888/cheyooh_app/i.ashx";//测试服务器
//		return NetLib.getServerUrl();
	}
	
	public static String getSecrectKey(){
      return "esietxo4z4qikydlfv3qxo1m0nmjukp7"; //后期需要使用lib形式进行封装
//		return NetLib.getSecrectKey();
	}
	
	/**
	 * 获取机器的IMEI
	 * 
	 * @return
	 */
	public static String getIMEI(Context context) {
		String IMEI = "";
		try {
			TelephonyManager tm = (TelephonyManager) context
					.getSystemService(Context.TELEPHONY_SERVICE);
			IMEI = tm.getDeviceId();

			if (IMEI != null
					&& (IMEI.length() < 15 
					|| IMEI.equals("004999010640000")
					|| IMEI.equals("000000000000000") 
					|| IMEI.equals("null"))) {
				return null;
			}

			return IMEI;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static String getVersionName(Context cx) {
		String packName = cx.getPackageName();
		PackageInfo pinfo = null;
		try {
			pinfo = cx.getPackageManager().getPackageInfo(packName, 0);
		} catch (NameNotFoundException e) {

			e.printStackTrace();
		}
		if (pinfo != null)
			return pinfo.versionName;
		else
			return null;
	}
	
	public static String getChannelNumber(Context c) {
		try {
			ApplicationInfo ai = c.getPackageManager().getApplicationInfo(
					c.getPackageName(), PackageManager.GET_META_DATA);
			Bundle bundle = ai.metaData;
			String channelNumber = bundle.getString("CHEYOOH_CHANNEL");
			return channelNumber;

		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 获取Android本机MAC
	 * 
	 * @return
	 */
	public static String getLocalMacAddress(Context context) {
		WifiManager wifi = (WifiManager) context
				.getSystemService(Context.WIFI_SERVICE);
		WifiInfo info = wifi.getConnectionInfo();
		return info.getMacAddress();
	}
	
	/**
	 * 获取APK签名验证字符串
	 * @return
	 */
	public static String getAppKeyMd5(Context context){
		try{
			PackageManager pm = context.getPackageManager();
			PackageInfo pi = pm.getPackageInfo(context.getPackageName(), PackageManager.GET_SIGNATURES);
			
			Signature signature = pi.signatures[0];
			
			byte[] sign = signature.toByteArray();
		    CertificateFactory cf = CertificateFactory.getInstance("X.509");
		    java.security.cert.Certificate cert = cf.generateCertificate(new ByteArrayInputStream(sign));
			
	        MessageDigest md = MessageDigest.getInstance("MD5");
	        
	        byte[] digest = md.digest(cert.getEncoded());
	        String md5 = toHexString(digest);
			
//	        Log.d(TAG, md5);
//	        md5 = "7CCC9B791E78A8F36745C796A0225052";
//			int ret = NetLib.get(md5);
//	        Log.d(TAG, "ret:" + ret);
			
			return md5;
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	private static void byte2hex(byte b, StringBuffer buf) {
        char[] hexChars = { '0', '1', '2', '3', '4', '5', '6', '7', '8',
                            '9', 'A', 'B', 'C', 'D', 'E', 'F' };
        int high = ((b & 0xf0) >> 4);
        int low = (b & 0x0f);
        buf.append(hexChars[high]);
        buf.append(hexChars[low]);
    }

    /**
     * Converts a byte array to hex string
     */
    private static String toHexString(byte[] block) {
        StringBuffer buf = new StringBuffer();
        int len = block.length;
        for (int i = 0; i < len; i++) {
             byte2hex(block[i], buf);
        }
        return buf.toString();
    }

}

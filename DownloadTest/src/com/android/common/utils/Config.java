package com.android.common.utils;

import android.os.Environment;

/**
 * 类说明：
 * 
 * @author xinhui.cheng
 * @date 2013-4-7
 * @version 1.0
 */
public class Config {

	/** SD卡目录 */
	public static String SDCARD_DIR = Environment.getExternalStorageDirectory()
			.getAbsolutePath();

	
	/** 下载根目录 */
	public static String APK_DOWNLOAD_DIR = SDCARD_DIR + "/download/.apks";

	
}

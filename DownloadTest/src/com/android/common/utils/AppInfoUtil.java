package com.android.common.utils;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.net.Uri;

/**
 * 根据apk获取app的名称和icon
 * */
public class AppInfoUtil {

	/**
	 * 根据apk获取app的名称和icon
	 * 
	 * @param cx
	 * @param apkPath
	 *            apk的名字
	 * @return AppInfoModel 返回实体appInfoModel对象
	 * */
	public static AppInfoModel getApkInfo(Context cx, String apkPath) {
		AppInfoModel apkInfoModel = null;
		PackageManager pm = cx.getPackageManager();
		
		PackageInfo pkgInfo = pm.getPackageArchiveInfo(Uri.decode(apkPath), PackageManager.GET_ACTIVITIES);
		if (pkgInfo != null) {
			ApplicationInfo appInfo = pkgInfo.applicationInfo;
			/* 必须加这两句，不然下面icon获取是default icon而不是应用包的icon */
			appInfo.sourceDir = apkPath;
			appInfo.publicSourceDir = apkPath;
			String appName = pm.getApplicationLabel(appInfo).toString();// 得到应用名
			Drawable icon1 = pm.getApplicationIcon(appInfo);// 得到图标信息
			apkInfoModel = new AppInfoModel(appName, icon1);
		}
		return apkInfoModel;
	}
}
